export class IncompleteGame {

  constructor(
    public hscore: number,
    public agoalds: number,
    public is_final: number,
    public ateam: String,
    public ateamid: number,
    public round: number,
    public updated: String,
    public hteam: String,
    public hbehinds: number,
    public complete: number,
    public winner: String,
    public year: number,
    public ascore: number,
    public hteamid: number,
    public abehinds: number,
    public date: String,
    public id: number,
    public hgoals: number,
    public winnerteamid: number,
    public tz: String,
    public is_grand_final: number,
    public venue: String
  ) {}

}
