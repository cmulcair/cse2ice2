import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';

@Component({
  selector: 'app-user2',
  templateUrl: './user2.component.html',
  styleUrls: ['./user2.component.css']
})
export class User2Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeam2 = this.teampickService.getTeam();
  theGames: Game[];

  output: String[] = [''];
  tempString: String;

  results() {
    for (let i = 0; i < this.theGames.length; i++) {
      if (this.theGames[i].complete === 100){
        if (this.theGames[i].ateam === this.theTeam2.name || this.theGames[i].hteam === this.theTeam2.name){
          this.tempString = 'Round ' + this.theGames[i].round + ' winner was: ' + this.theGames[i].winner;
          this.output.push(this.tempString);
        }
      }
    }
  }

  ngOnInit() {
    this.getGames();
  }

  getGames(): void {
    this.dataService.getGames().subscribe(
      (temp) => {
        this.theGames = temp;
        this.results();
      });
  }
}
