import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';

@Component({
  selector: 'app-user8',
  templateUrl: './user8.component.html',
  styleUrls: ['./user8.component.css']
})
export class User8Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeam8 = this.teampickService.getTeam();
  theGames: Game[];

  output: String[] = [''];
  tempString;
  tempString1;
  tempString2;

  results() {
    for (let i = 0; i < this.theGames.length; i++) {
      if(this.theGames[i].hteam === this.theTeam8.name){
        if(this.theGames[i].complete === 100) {
          this.tempString1 = 'Round ' + this.theGames[i].round + ' ' + this.theGames[i].hteam + ' played at ';
          this.tempString2 = '' + this.theGames[i].venue;
          this.tempString = this.tempString1 + '' + this.tempString2;
        }
        if(this.theGames[i].complete === 0) {
          this.tempString1 = 'Round ' + this.theGames[i].round + ' ' + this.theGames[i].hteam + ' will play at ';
          this.tempString2 = '' + this.theGames[i].venue;
          this.tempString = this.tempString1 + '' + this.tempString2;
        }
        this.output.push(this.tempString);
      }
    }
  }

  ngOnInit() {
    this.getGames();
  }

  getGames(): void {
    this.dataService.getGames().subscribe(
      (temp) => {
        this.theGames = temp;
        this.results();
      });
  }

}
