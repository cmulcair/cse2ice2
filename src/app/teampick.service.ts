import { Injectable } from '@angular/core';
import { Team } from './aflteam';

@Injectable({
  providedIn: 'root'
})
export class TeampickService {
  theSelectedTeam: Team;

  getTeam() {
    return this.theSelectedTeam;
  }

  setTeam(team: Team) {
    this.theSelectedTeam = team;
  }

  constructor() { }
}
