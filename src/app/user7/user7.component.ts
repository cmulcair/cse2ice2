import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';

interface TeamResults {
  name: string;
  id: number;
  win: number;
  lose: number;
  draw: number;
  ladderP: number;
  goal: number;
  point: number;
  score: number;
  scoreA: number;
  percent: string;
}

@Component({
  selector: 'app-user7',
  templateUrl: './user7.component.html',
  styleUrls: ['./user7.component.css']
})
export class User7Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeams: Team[];
  theGames: Game[];

  teamData: TeamResults;
  teamsData: TeamResults[] = [];

  output;

  results() {
    for (let i = 0; i < this.theTeams.length; i++) {
      this.teamData = {
        name: this.theTeams[i].name,
        id: this.theTeams[i].id,
        win: 0,
        lose: 0,
        draw: 0,
        ladderP: 0,
        goal: 0,
        point: 0,
        score: 0,
        scoreA: 0,
        percent: ''
      };
      this.teamsData.push(this.teamData);
    }
    // testing
    // this.output = this.teamsData;
    for (let x = 0; x < this.theGames.length; x++) {
      if (this.theGames[x].complete === 100){
        // home
        for (let h = 0; h < this.teamsData.length; h++) {

          if (this.theGames[x].hteamid === this.teamsData[h].id) {
            // win
            // goals
            // points
            // score for and against
            if (this.theGames[x].winnerteamid === this.theGames[x].hteamid) {
              this.teamsData[h].win += 1;
              this.teamsData[h].goal += this.theGames[x].hgoals;
              this.teamsData[h].point += this.theGames[x].hbehinds;
              this.teamsData[h].score += this.theGames[x].hgoals * 6 + this.theGames[x].hbehinds;
              this.teamsData[h].scoreA += this.theGames[x].agoals * 6 + this.theGames[x].abehinds;
              this.teamsData[h].ladderP += 4;
            }
            //lose
            if (this.theGames[x].winnerteamid !== this.theGames[x].hteamid) {
              this.teamsData[h].lose += 1;
              this.teamsData[h].goal += this.theGames[x].hgoals;
              this.teamsData[h].point += this.theGames[x].hbehinds;
              this.teamsData[h].score += this.theGames[x].hgoals * 6 + this.theGames[x].hbehinds;
              this.teamsData[h].scoreA += this.theGames[x].agoals * 6 + this.theGames[x].abehinds;
            }
            //draw
            if (this.theGames[x].hscore === this.theGames[x].ascore) {
              this.teamsData[h].draw += 1;
              this.teamsData[h].ladderP += 2;
            }
          }
        }
        // away
        for (let a = 0; a < this.teamsData.length; a++) {

          if (this.theGames[x].ateamid === this.teamsData[a].id) {
            // win
            // goals
            // points
            // score for and against
            if (this.theGames[x].winnerteamid === this.theGames[x].ateamid) {
              this.teamsData[a].win += 1;
              this.teamsData[a].goal += this.theGames[x].agoals;
              this.teamsData[a].point += this.theGames[x].abehinds;
              this.teamsData[a].score += this.theGames[x].agoals * 6 + this.theGames[x].abehinds;
              this.teamsData[a].scoreA += this.theGames[x].hgoals * 6 + this.theGames[x].hbehinds;
              this.teamsData[a].ladderP += 4;
            }
            //lose
            if (this.theGames[x].winnerteamid !== this.theGames[x].ateamid) {
              this.teamsData[a].lose += 1;
              this.teamsData[a].goal += this.theGames[x].agoals;
              this.teamsData[a].point += this.theGames[x].abehinds;
              this.teamsData[a].score += this.theGames[x].agoals * 6 + this.theGames[x].abehinds;
              this.teamsData[a].scoreA += this.theGames[x].hgoals * 6 + this.theGames[x].hbehinds;
            }
            //draw
            if (this.theGames[x].hscore === this.theGames[x].ascore) {
              this.teamsData[a].draw += 1;
              this.teamsData[a].ladderP += 2;
            }
          }
        }
      }
    }
    //percent
    for (let p = 0; p < this.teamsData.length; p++) {
      // this.teamsData[p].percent = Math.round(this.teamsData[p].score / this.teamsData[p].scoreA * 100);
      this.teamsData[p].percent = (this.teamsData[p].score / this.teamsData[p].scoreA * 100).toFixed(2);
    }
    this.output =  (this.teamsData);
    this.output = this.output.sort((a, b) => (a.ladderP < b.ladderP) ? 1 : -1);
  }

  ngOnInit() {
    this.getTeams();
  }

  getTeams(): void {
    this.dataService.getTeams().subscribe(
      (temp) => {
        this.theTeams = temp;
        this.getGames();
      });
  }

  getGames(): void {
    this.dataService.getGames().subscribe(
      (temp) => {
        this.theGames = temp;
        this.results();
      });
  }

}
