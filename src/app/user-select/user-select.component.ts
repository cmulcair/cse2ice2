import { Component, OnInit, Input } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';

@Component({
  selector: 'app-user-select',
  templateUrl: './user-select.component.html',
  styleUrls: ['./user-select.component.css']
})
export class UserSelectComponent implements OnInit {
  @Input() team: Team;

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

}
