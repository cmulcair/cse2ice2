import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { TeamSelectComponent } from './teamselect/teamselect.component';
import { UserSelectComponent } from './user-select/user-select.component';
import { User1Component } from './user1/user1.component';
import { User2Component } from './user2/user2.component';
import { User3Component } from './user3/user3.component';
import { User4Component } from './user4/user4.component';
import { User5Component } from './user5/user5.component';
import { User6Component } from './user6/user6.component';
import { User7Component } from './user7/user7.component';
import { User8Component } from './user8/user8.component';
import { DataService } from './data.service';
import { AppRoutingModule } from './app-routing.module';
import { TeampickService } from './teampick.service';

@NgModule({
  declarations: [
    AppComponent,
    TeamSelectComponent,
    UserSelectComponent,
    User1Component,
    User2Component,
    User3Component,
    User4Component,
    User5Component,
    User6Component,
    User7Component,
    User8Component,
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, AppRoutingModule
  ],
  providers: [DataService, TeampickService],
  bootstrap: [AppComponent]
})
export class AppModule { }
