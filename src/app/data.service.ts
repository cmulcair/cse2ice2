import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Team } from './aflteam';
import { Tip } from './afltip';
import { Game } from './aflgame';
import { Source } from './aflsource';
import { IncompleteGame } from './aflincompletegame';

import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http: HttpClient) { }

  ngOnIniy() {
    this.getTeams();
    this.getTips();
    this.getSources();
    this.getIncompleteGames();
    this.getGames();
  }

  //getTeams
  getTeams() : Observable<Team[]> {
    return this.http.get<Team[]>('https://api.squiggle.com.au/?q=teams').pipe(
      map((data: any) => data.teams.map((item: any) => new Team(
        item.logo,
        item.id,
        item.name,
        item.abbrev
      )))
    );
  }

  //getTips
  getTips() : Observable<Tip[]> {
    return this.http.get<Tip[]>('https://api.squiggle.com.au/?q=tips;year=2019').pipe(
      //return this.http.get<Tip[]>('https://api.squiggle.com.au/?q=tips').pipe(
      map((data: any) => data.tips.map((item: any) => new Tip(
        item.confidence,
        item.bits,
        item.gameid,
        item.ateamid,
        item.venue,
        item.year,
        item.correct,
        item.date,
        item.updated,
        item.hteam,
        item.tipteamid,
        item.margin,
        item.err,
        item.tip,
        item.ateam,
        item.source,
        item.sourceid,
        item.hconfidence,
        item.hteamid,
        item.round
      )))
    );
  }

  //getSources
  getSources() : Observable<Source[]>{
    return this.http.get<Source[]>('https://api.squiggle.com.au/?q=sources').pipe(
      map((data: any) => data.sources.map((item: any) => new Source(
        item.id,
        item.url,
        item.name
      )))
    );
  }

  getIncompleteGames() : Observable<IncompleteGame[]>{
    return this.http.get<IncompleteGame[]>('https://api.squiggle.com.au/?q=games;year=2019;complete=0').pipe(
      map((data: any) => data.games.map((item: any) => new IncompleteGame(
        item.hscore,
        item.agoalds,
        item.is_final,
        item.ateam,
        item.ateamid,
        item.round,
        item.updated,
        item.hteam,
        item.hbehinds,
        item.complete,
        item.winner,
        item.year,
        item.ascore,
        item.hteamid,
        item.abehinds,
        item.date,
        item.id,
        item.hgoals,
        item.winnerteamid,
        item.tz,
        item.is_grand_final,
        item.venue
      )))
    );
  }


  //getGames
  getGames() : Observable<Game[]>{
  return this.http.get<Game[]>('https://api.squiggle.com.au/?q=games;year=2019').pipe(
    map((data: any) => data.games.map((item: any) => new Game(
      item.complete,
      item.is_grand_final,
      item.tz,
      item.hbehinds,
      item.ateam,
      item.winnerteamid,
      item.hgoals,
      item.updated,
      item.round,
      item.is_final,
      item.hscore,
      item.abehinds,
      item.winner,
      item.ascore,
      item.hteam,
      item.ateamid,
      item.venue,
      item.hteamid,
      item.agoals,
      item.year,
      item.date,
      item.id
    )))
  );
  }


}
