import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';

@Component({
  selector: 'app-user5',
  templateUrl: './user5.component.html',
  styleUrls: ['./user5.component.css']
})
export class User5Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeam5 = this.teampickService.getTeam();
  theRivalTeam: Team;
  theGames: Game[];
  theTeams: Team[];

  output: String[] = [''];
  tempS1;
  tempS2;
  tempS3;
  tempS4;
  tempS5;
  tempS6;

  results() {
    for (let i = 0; i < this.theGames.length; i++) {
      if (this.theGames[i].ateam === this.theTeam5.name || this.theGames[i].hteam === this.theTeam5.name){
        if (this.theGames[i].ateam === this.theRivalTeam.name || this.theGames[i].hteam === this.theRivalTeam.name){
          if (this.theGames[i].complete === 100) {
            this.tempS1 = 'Round ' + this.theGames[i].round + ' played on ' + this.theGames[i].date;
            this.output.push(this.tempS1);
            this.tempS2 = 'Home: ' + this.theGames[i].hteam + ' on ';
            this.tempS3 = this.theGames[i].hbehinds + ' ' + this.theGames[i].hgoals + ' ' + this.theGames[i].hscore;
            this.output.push(this.tempS2 + '' + this.tempS3);
            this.tempS4 = 'Away: ' + this.theGames[i].ateam + ' on ';
            this.tempS5 = this.theGames[i].abehinds + ' ' + this.theGames[i].agoals + ' ' + this.theGames[i].ascore;
            this.output.push(this.tempS4 + '' + this.tempS5);
            this.tempS6 = 'The winner was: ' + this.theGames[i].winner;
            this.output.push(this.tempS6);
          } else if (this.theGames[i].complete === 0) {
            this.tempS1 = 'Round ' + this.theGames[i].round + ' will be played on ' + this.theGames[i].date;
            this.output.push(this.tempS1);
            this.tempS2 = 'Home: ' + this.theGames[i].hteam;
            this.output.push(this.tempS2);
            this.tempS3 = 'Away: ' + this.theGames[i].ateam;
            this.output.push(this.tempS3);
          }
        }
      }
    }
  }

  ngOnInit() {
    this.getTeams();
  }

  onClick(team: Team): void {
    this.theRivalTeam = team;
    this.results();
  }

  getTeams(): void {
    this.dataService.getTeams().subscribe(
      (temp) => {
        this.theTeams = temp;
        this.getGames();
      });
  }

  getGames(): void {
    this.dataService.getGames().subscribe(
      (temp) => {
        this.theGames = temp;
      });
  }
}
