import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { User6Component } from './user6.component';

describe('User6Component', () => {
  let component: User6Component;
  let fixture: ComponentFixture<User6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ User6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(User6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
