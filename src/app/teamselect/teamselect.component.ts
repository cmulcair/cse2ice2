import { Component, OnInit } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';

@Component({
  selector: 'app-teamselect',
  templateUrl: './teamselect.component.html',
  styleUrls: ['./teamselect.component.css']
})
export class TeamSelectComponent implements OnInit {

  selectedTeam: Team;

  teams: Team[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  ngOnInit() {
    this.getTeams();
  }

  onSelect(team: Team): void {
    this.selectedTeam = team;
    this.teampickService.setTeam(team);
  }

  getTeams(): void {
    this.dataService.getTeams().subscribe(temp => this.teams = temp);
  }

}
