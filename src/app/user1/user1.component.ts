import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Tip } from '../afltip';
import { map } from 'rxjs/operators';
import { Source } from '../aflsource';
import { IncompleteGame } from '../aflincompletegame';

@Component({
  selector: 'app-user1',
  templateUrl: './user1.component.html',
  styleUrls: ['./user1.component.css']
})
export class User1Component implements OnInit {
  @Input() team: Team;
  @Input() tips: Tip;
  @Input() sources: Source;
  @Input() incompleteGames: IncompleteGame;


  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeam1 = this.teampickService.getTeam();
  theTips: Tip[];
  theSources: Source[];
  theIncompleteGames: IncompleteGame[];
  theTeams: Team[];

  sourceName: String;
  tipId: number;
  tipName: String;
  output: String[] = [''];
  tempString: String;

  results(){
    for (let i = 0; i < this.theTips.length; i++){
        if (this.theTips[i].round === this.theIncompleteGames[0].round) {
          if (this.theTips[i].hteam === this.theTeam1.name || this.theTips[i].ateam === this.theTeam1.name) {
            this.sourceName = this.theTips[i].source;
            this.tipId = this.theTips[i].tipteamid;

            for (let y = 0; y < this.theTeams.length; y++){
              if (this.tipId === this.theTeams[y].id){
                this.tipName = this.theTeams[y].name;
              }
            }
            this.tempString = 'For round ' + this.theTips[i].round + ', ' + this.sourceName + ' tips ' + this.tipName;
            this.output.push(this.tempString);
          }
        }
    }
  }


  ngOnInit() {
     this.getTips();
    //  this.getSources();
    //  this.getIncompleteGames();
  }
  getTips(): void {
    this.dataService.getTips().subscribe(
      (temp) => {
        this.theTips = temp;
        this.getSources();
      });
  }
  getSources(): void {
    this.dataService.getSources().subscribe(
      (temp) => {
        this.theSources = temp;
        this.getIncompleteGames();
      });
  }
  getIncompleteGames(): void {
    this.dataService.getIncompleteGames().subscribe(
      (temp) => {
       this.theIncompleteGames = temp;
       this.getTeams();
    });
  }
  getTeams(): void {
    this.dataService.getTeams().subscribe(
      (temp) => {
        this.theTeams = temp;
        this.results();
      });
  }

}
