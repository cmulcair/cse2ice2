import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';

@Component({
  selector: 'app-user4',
  templateUrl: './user4.component.html',
  styleUrls: ['./user4.component.css']
})
export class User4Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(private dataService: DataService, private teampickService: TeampickService) { }

  theTeam4 = this.teampickService.getTeam();
  theGames: Game[];

  output: String[] = [''];
  tempString: String;
  tempString2: String;
  tempStringWL: String;

  results() {
    for (let i = 0; i < this.theGames.length; i++) {
      if (this.theGames[i].complete === 100){
        if (this.theGames[i].ateam === this.theTeam4.name ||  this.theGames[i].hteam === this.theTeam4.name){

          if(this.theGames[i].winner === this.theTeam4.name){
            this.tempStringWL = 'win';
          } else {
            this.tempStringWL = 'loss';
          }
          this.tempString = 'Round ' + this.theGames[i].round + ' venue was ' + this.theGames[i].venue;
          this.tempString2 = this.tempString + ' and the result was a ' + this.tempStringWL;
          this.output.push(this.tempString2);
        }
      }
    }
  }

  ngOnInit() {
    this.getGames();
  }

  getGames(): void {
    this.dataService.getGames().subscribe(
      (temp) => {
        this.theGames = temp;
        this.results();
      });
  }


}
