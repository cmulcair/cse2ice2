import { Component, OnInit, Input, Output } from '@angular/core';
import { Team } from '../aflteam';
import { DataService } from '../data.service';
import { TeampickService } from '../teampick.service';
import { Game } from '../aflgame';
import { IncompleteGame } from '../aflincompletegame';

interface Round {
  score: number;
  theTeam: String
};

@Component({
  selector: 'app-user6',
  templateUrl: './user6.component.html',
  styleUrls: ['./user6.component.css']
})



export class User6Component implements OnInit {
  @Input() team: Team;
  @Input() game: Game[];

  constructor(
    private dataService: DataService,
    private teampickService: TeampickService
  ) {}

  theGames: Game[];
  theIncompleteGames: IncompleteGame[];
  lastRound;
  output: Round[] = [];
  sortedOutput: Round[] = [];
  tempObj: Round;


  results() {
    this.lastRound = this.theIncompleteGames[0].round - 1;
    for (let i = 0; i < this.theGames.length; i++) {
      if (this.theGames[i].round === this.lastRound) {
        this.tempObj = {score: this.theGames[i].hscore, theTeam: this.theGames[i].hteam};
        this.output.push(this.tempObj);
        this.tempObj = {score: this.theGames[i].ascore, theTeam: this.theGames[i].ateam};
        this.output.push(this.tempObj);
      }
    }
    this.sortedOutput = this.output.sort((a, b) => (a.score < b.score) ? 1 : -1);
  }

  ngOnInit() {
    this.getGames();
  }

  getGames(): void {
    this.dataService.getGames().subscribe(temp => {
      this.theGames = temp;
      this.getIncompleteGames();
    });
  }
  getIncompleteGames(): void {
    this.dataService.getIncompleteGames().subscribe(temp => {
      this.theIncompleteGames = temp;
      this.results();
    });
  }
}
