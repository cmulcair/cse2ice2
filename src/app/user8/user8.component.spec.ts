import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { User8Component } from './user8.component';

describe('User8Component', () => {
  let component: User8Component;
  let fixture: ComponentFixture<User8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ User8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(User8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
