export class Source {

  constructor(
    public id: number,
    public url: String,
    public name: String
  ) {}

}
