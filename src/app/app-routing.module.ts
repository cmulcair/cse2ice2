import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { User1Component } from './user1/user1.component';
import { User2Component } from './user2/user2.component';
import { User3Component } from './user3/user3.component';
import { User4Component } from './user4/user4.component';
import { User5Component } from './user5/user5.component';
import { User6Component } from './user6/user6.component';
import { User7Component } from './user7/user7.component';
import { User8Component } from './user8/user8.component';
import { TeamSelectComponent } from './teamselect/teamselect.component';
import { UserSelectComponent } from './user-select/user-select.component';

const routes: Routes = [
  { path: 'teamSelect', component: TeamSelectComponent },
  { path: 'userselect', component: UserSelectComponent },
  { path: 'user1', component: User1Component },
  { path: 'user2', component: User2Component },
  { path: 'user3', component: User3Component},
  { path: 'user4', component: User4Component},
  { path: 'user5', component: User5Component},
  { path: 'user6', component: User6Component},
  { path: 'user7', component: User7Component},
  { path: 'user8', component: User8Component},
  { path: '', component: TeamSelectComponent},
  { path: '**', component: TeamSelectComponent}
];

// created with file
// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
// })
// copied from uni example
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
